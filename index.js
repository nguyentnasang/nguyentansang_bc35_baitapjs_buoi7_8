var numbers = [];
document.getElementById("btn").onclick = function () {
  var number = document.getElementById("number");
  if (number.value == 0) {
    return;
  }
  numbers.push(number.value * 1);
  number.value = "";

  document.querySelector(".resultN").innerHTML = `Thêm số: ${numbers}`;
  //   câu 1
};
document.querySelector("#btn1").onclick = function () {
  var sum1 = 0;
  for (var i = 0; i < numbers.length; i++) {
    var numberArray = numbers[i] * 1;
    if (numberArray >= 0) {
      sum1 += numberArray;
    }
  }
  document.querySelector(".result1").innerHTML = `Tổng số dương là: ${sum1}`;
};
// câu 2
document.querySelector("#btn2").onclick = function () {
  var sum2 = 0;
  for (var i = 0; i < numbers.length; i++) {
    var numberArray = numbers[i] * 1;
    if (numberArray >= 0) {
      sum2++;
    }
  }
  document.querySelector(".result2").innerHTML = ` Số dương là: ${sum2}`;
};
// câu 3
document.querySelector("#btn3").onclick = function () {
  var pennant = numbers[0];
  for (var i = 0; i < numbers.length; i++) {
    var numberArray = numbers[i] * 1;
    if (pennant > numberArray) {
      pennant = numberArray;
    } else {
      pennant = pennant;
    }
  }
  document.querySelector(".result3").innerHTML = ` Số nhỏ nhất là ${pennant}`;
};
// câu 4
document.querySelector("#btn4").onclick = function () {
  var pennant = numbers[0];
  for (var i = 0; i < numbers.length; i++) {
    var numberArray = numbers[i];

    if (numberArray > 0) {
      if (pennant > numberArray) {
        pennant = numberArray;
      } else {
        pennant = pennant;
      }
    }
  }
  document.querySelector(
    ".result4"
  ).innerHTML = ` Số nguyên dương nhỏ nhât là: ${pennant}`;
};
// câu 5
document.querySelector("#btn5").onclick = function () {
  var pennant = "";
  for (var i = 0; i < numbers.length; i++) {
    var numberArray = numbers[i];
    if (numberArray % 2 == 0) {
      pennant = numberArray;
    }
  }
  document.querySelector(
    ".result5"
  ).innerHTML = ` Số chẳn cuối cùng là: ${pennant}`;
};
// câu 6
document.querySelector("#btn6").onclick = function () {
  var num1 = document.querySelector("#num1").value * 1;
  var num2 = document.querySelector("#num2").value * 1;
  var sum = [];
  for (var i = 0; i < numbers.length; i++) {
    var numberArray = numbers[i];
    sum.push(numberArray);
  }
  var bien2 = sum[num2];
  sum[num2] = sum[num1];
  sum[num1] = bien2;
  document.querySelector(
    ".result6"
  ).innerHTML = ` Thứ tự khi đỏi chỗ là: ${sum}`;
};
// câu 7
document.querySelector("#btn7").onclick = function () {
  var pennant = [];
  for (var i = 0; i < numbers.length; i++) {
    pennant.push(numbers[i]);
  }
  pennant.sort(function (a, b) {
    return a - b;
  });
  document.querySelector(".result7").innerHTML = pennant;
};
// câu 8
document.querySelector("#btn8").onclick = function () {
  var array = [];
  for (var i = 0; i < numbers.length; i++) {
    var dem = 0;
    if (numbers[i] > 1) {
      for (var j = 0; j < numbers.length; j++) {
        if (numbers[i] % numbers[j] == 0 && numbers[j] != 1) {
          dem++;
        }
      }
    }
    if (dem == 1) {
      array.push(numbers[i]);
    }
  }
  document.querySelector(".result8").innerHTML = array[0];
};
// câu 9
document.querySelector("#btn9").onclick = function () {
  var dem = 0;
  for (var i = 0; i < numbers.length; i++) {
    if (Number.isInteger(numbers[i])) {
      dem++;
    }
  }
  document.querySelector(".result9").innerHTML = dem;
};
// câu 10
document.querySelector("#btn10").onclick = function () {
  var negativenum = 0;
  var positivenum = 0;
  for (var i = 0; i < numbers.length; i++) {
    if (numbers[i] < 0) {
      negativenum++;
    } else {
      positivenum++;
    }
  }
  if (positivenum > negativenum) {
    document.querySelector(".result10").innerHTML = ` Số dương > Số âm`;
  } else if (positivenum < negativenum) {
    document.querySelector(".result10").innerHTML = ` Số âm > Số dương`;
  } else {
    document.querySelector(".result10").innerHTML = ` Số âm = Số dương`;
  }
};
